# advanced-ansible
Repository for Advanced Ansible Training

## Checklist

- Dynamic inventories ([Azure](https://raw.githubusercontent.com/ansible/ansible/stable-2.8/contrib/inventory/azure_rm.py) & [AWS](https://raw.githubusercontent.com/ansible/ansible/stable-2.8/contrib/inventory/ec2.py))
- [AWX](https://github.com/ansible/awx) (open-source Ansible Tower-like platform)
- [Ansible-lint](https://github.com/ansible/ansible-lint) & CI in playbook/roles development
- [Ansible molecule](https://molecule.readthedocs.io/en/stable/) (testing)