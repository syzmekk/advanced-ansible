# Ansible AWX

## Prerequisites (Ubuntu)

### Ansible

Don't be silly, that's Ansible `advanced` workshop - it's already installed :wink:.

### Python

One of Ansible dependencies is `python-minimal` package. Yet, as you remember, we must have Python `pip` package manager.

```sh
$ sudo apt update && sudo apt install python-pip -y
```

In case Python is also absent ([possimpible](https://www.urbandictionary.com/define.php?term=Possimpible)), run

```sh
$ sudo apt install python-minimal -y
```

### Docker

A recent version!

- Uninstall old versions

```sh
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

- Update apt repos

```sh
$ sudo apt-get update
```

- Install necessary packages

```sh
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
```

- Add Docker's GPG key

```sh
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

- Set up `stable` repository

```sh
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

- Install Docker CE

```sh
$ sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io -y
```

- Verify that Docker is istalled correctly by running the `hello-world` image

```sh
$ sudo docker run hello-world

#Fragment of expected output
Hello from Docker!
This message shows that your installation appears to be working correctly.
```

### docker Python module

- Get rid of `docker-py` module, because of its incompatibility.

```sh
$ sudo -H python -m pip uninstall docker-py
```

- Install required module

```sh
$ sudo -H python -m pip install docker docker-compose
```

### GNU make

You should have it, but in case you do not - [download it](http://ftp.gnu.org/gnu/make/make-4.2.tar.gz) and add it to `PATH` executable directory. Don't forget about permissions.

To check if binary is present & working, type

```sh
$ make -v
```

to see its version.

### Git

Same as above. You should make it!

### Node.js `10.x LTS`

- Run setup script from offical NodeSource site

```sh
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
```

- Install Node.js

```sh
$ sudo apt-get install nodejs -y
```

### NPM `6.x LTS`

If you installed `Node.js` from NodeSource successfully, you should have `npm` as well.

Try it

```sh
$ npm -v
```

## Installation

There are three different deployment platforms
- Kubernetes
- Openshift
- Docker-compose

and we'll use last one.

- Clone repo

```sh
$ git clone https://github.com/ansible/awx.git
```

- Change dir into awx/installer

```sh
$ cd awx/installer
```

- Generate the new secret key for the AWX

```sh
$ openssl rand -hex 32
```

and copy it to your note.

- Edit `inventory` config file

```sh
$ nano inventory
```

- Change the `postgres_data_dir` to the `/var/lib/pgdocker` directory

- Now make sure the `docker_compose_dir` is located to the `/var/lib/awx` directory

- Change the credentials for the `pg_password`, `rabbitmq_password`, `rabbitmq_erlang_cookie`, `admin_user` and `admin_password` with your own password credentials

- Change the `secret_key` generated a few step before

- Uncomment the `project_data_dir` and leave the value as default

__*If you're running something on default HTTP port `(80)`, change `host_port`.*__

- Check AWX config once again

```sh
$ grep -v '^#' inventory
```

- Install the Ansible AWX

```sh
$ sudo ansible-playbook -i inventory install.yml
```

- Change dir into `/var/lib/awx` (`docker_compose_dir`) and run `docker ps` command

```sh
$ cd /var/lib/awx

$ sudo docker ps

#Expected output
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                                                 NAMES
22b31c759bea        ansible/awx_task:4.0.0       "/tini -- /bin/sh -c…"   2 minutes ago       Up 2 minutes        8052/tcp                                              awx_task
74e6b7ad4bf9        ansible/awx_web:4.0.0        "/tini -- /bin/sh -c…"   2 minutes ago       Up 2 minutes        0.0.0.0:80->8052/tcp                                  awx_web
2144f0b1b008        ansible/awx_rabbitmq:3.7.4   "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        4369/tcp, 5671-5672/tcp, 15671-15672/tcp, 25672/tcp   awx_rabbitmq
6a1ee4ea4a91        memcached:alpine             "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        11211/tcp                                             awx_memcached
7c0ea2e0acf6        postgres:9.6                 "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        5432/tcp                                              awx_postgres
```

## Testing

### Log in

Default AWX credentials are `admin`/`password`.

### Try to set up dynamic Azure inventory in AWX

- Click `Inventory Scripts` on left nav bar;
- Click `green plus button` on right side;
- Give it a name, and paste Python script which we have been using previously. Then, save it;
- Next, navigate to `Credentials`, and click `green plus button` once again;
- Give it a name, in `CREDENTIAL TYPE` select `Microsoft Azure Resource Manager`, and save it;
- Then, go to `Invetories`, click `green plus button` one more time, and select `Inventory`;
- Give it a name, save it, and then select `SOURCES` tab on top;
- Again `green plus button`, name, and `Microsoft Azure Resource Manager` as a source;
- The `Source details` will appear. Click a loupe, and select the credentials you've created before. Then save.;
- Scroll to the bottom, find your `Source`, under `Actions` section find `sync button`, and click it.;

Voilà, you set up the dynamic inventory script for Azure services. Now, you can navigate to `Inventories` on nav bar, select `HOSTS` tab on top, and see your hosts generated by the script.

### Jobs

In `Jobs` section you can find your `Management jobs` and `Inventory syncs`. Click it and see the verbose log (depends on verbosity level).