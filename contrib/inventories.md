# Inventories

## Azure

### Set up your Ansible master node

- Update repos

```sh
$ sudo apt update
```

- Install python package manager

```sh
$ sudo apt install python-pip -y
```

- Install python dependencies

```sh
# Run command using sudo with -H flag (in case of your second thoughts of machine behavior, see man page `man sudo`)

$ sudo -H python -m pip install 'ansible[azure]'
```

- Download python script (ver. 2.8 stable - latest)

```sh
$ wget https://raw.githubusercontent.com/ansible/ansible/stable-2.8/contrib/inventory/azure_rm.py
```

- Change file permissions

```sh
$ sudo chmod +x azure_rm.py
```

- Install azure-cli (doesn't matter if you use WSL or Azure VM)

```sh
# Ubuntu/Debian

$ curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

# CentOS/RHEL/Fedora

$ sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
$ sudo sh -c 'echo -e "[azure-cli]\nname=Azure CLI\nbaseurl=https://packages.microsoft.com/yumrepos/azure-cli\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'
$ sudo yum install azure-cli
```

- Create Azure credentials file

```sh
$ nano ~/.azure/credentials
```

and fill it with your credentials as in provided template:

```ini
[default]
subscription_id=<your-subscription_id>
client_id=<security-principal-appid>
secret=<security-principal-password>
tenant=<security-principal-tenant>
```

### `Don't know how to get your secrets?`

- subscription_id
    - Go to [Azure portal](https://portal.azure.com/)
    - Navigate to `All services`
    - Select `Subscriptions`, and copy its ID
- client_id, secret, and tenant
    - See the [docs](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal/)

### List your VMs

```sh
$ python ./azure_rm.py --list | jq
# Expected output
{
  "westeurope": [
    "master"
  ],
  "_meta": {
    "hostvars": {
      "master": {
        "powerstate": "running",
        "resource_group": "ansible",
        "tags": null,
        "image": {
          "sku": "18.04-LTS",
          "publisher": "Canonical",
          "version": "latest",
          "offer": "UbuntuServer"
        },
        "public_ip_alloc_method": "Static",
        "os_disk": {
          "operating_system_type": "linux",
          "name": "master_disk1_somechars"
        },
        "provisioning_state": "Succeeded",
        "public_ip": "52.236.xxx.xx",
        "public_ip_name": "master-ip",
        "private_ip": "10.0.0.4",
        "computer_name": "master",
        "public_ip_id": "/subscriptions/your_subscription_id/resourceGroups/ansible/providers/Microsoft.Network/publicIPAddresses/master-ip",
        "security_group_id": "/subscriptions/your_subscription_id/resourceGroups/ansible/providers/Microsoft.Network/networkSecurityGroups/master-nsg",
        "ansible_host": "52.236.xxx.xx",
        "name": "master",
        "fqdn": null,
        "id": "/subscriptions/your_subscription_id/resourceGroups/ANSIBLE/providers/Microsoft.Compute/virtualMachines/master",
        "virtual_machine_size": "Standard_B2s",
        "network_interface_id": "/subscriptions/your_subscription_id/resourceGroups/ansible/providers/Microsoft.Network/networkInterfaces/master787",
        "private_ip_alloc_method": "Dynamic",
        "location": "westeurope",
        "mac_address": "00-0D-3A-46-XX-XX",
        "security_group": "master-nsg",
        "network_interface": "master787",
        "type": "Microsoft.Compute/virtualMachines",
        "plan": null
      }
    }
  },
  "master-nsg": [
    "master"
  ],
  "ansible": [
    "master"
  ],
  "linux": [
    "master"
  ],
  "azure": [
    "master"
  ]
}
```

## AWS

### Set up your Ansible master node

- Update repos

```sh
$ sudo apt update
```

- Install python package manager

```sh
$ sudo apt install python-pip -y
```

- Create file requirements.txt with packages listed below

```txt
ansible
boto
boto3
paramiko
PyYAML
Jinja2
httplib2
six
```

- Install python dependencies

```sh
# Run command using sudo with -H flag (in case of your second thoughts of machine behavior, see man page `man sudo`)

$ sudo -H python -m pip install -r requirements.txt
```

- Download python script (ver. 2.8 stable - latest)

```sh
$ wget https://raw.githubusercontent.com/ansible/ansible/stable-2.8/contrib/inventory/ec2.py
```

- Change file permissions

```sh
$ sudo chmod +x ec2.py
```

- Export `secret` variables

```sh
$ export AWS_ACCESS_KEY_ID=<your_aws_access_key_id>
$ export AWS_SECRET_ACCESS_KEY=<your_aws_secret_access_key>
```

### `Don't know how to get your secrets?`

- Open the [AWS IAM console](https://console.aws.amazon.com/iam0/).

- In the navigation pane, choose `Users`, `Add user`.

- Create `User name` and check `Programmatic access` checkbox below. Then `Next: Permissions`. 

- On the `Set permissions` page, click `Attach existing policies directly`, and select `Power User` privileges (`PowerUserAccess` in search bar). Then `Next: Tags`.

- Give it some `Tags` (optional), click `Next: Review`.

- Finally, click `Create user`, and save your secrets or download `.csv` file. 

### List your instances

```sh
$ python ec2.py
# Expected output
{
  "_meta": {
    "hostvars": {
      "54.194.xx.xx": {
        "ansible_host": "54.194.xx.xx",
        "ec2__in_monitoring_element": false,
        "ec2_account_id": "265177130511",
        "ec2_ami_launch_index": "0",
        "ec2_architecture": "x86_64",
        "ec2_block_devices": {
          "sda1": "vol-0147634fdd981f7b5"
        },
        "ec2_client_token": "",
        "ec2_dns_name": "ec2-54-194-xx-xx.eu-west-1.compute.amazonaws.com",
        "ec2_ebs_optimized": false,
        "ec2_eventsSet": "",
        "ec2_group_name": "",
        "ec2_hypervisor": "xen",
        "ec2_id": "i-0cf2dc8949370619b",
        "ec2_image_id": "ami-08d658f84a6d84a80",
        "ec2_instance_profile": "",
        "ec2_instance_type": "t2.medium",
        "ec2_ip_address": "54.194.xx.xx",
        "ec2_item": "",
        "ec2_kernel": "",
        "ec2_key_name": "aws",
        "ec2_launch_time": "2019-06-11T12:28:24.000Z",
        "ec2_monitored": false,
        "ec2_monitoring": "",
        "ec2_monitoring_state": "disabled",
        "ec2_persistent": false,
        "ec2_placement": "eu-west-1a",
        "ec2_platform": "",
        "ec2_previous_state": "",
        "ec2_previous_state_code": 0,
        "ec2_private_dns_name": "ip-172-31-36-xxx.eu-west-1.compute.internal",
        "ec2_private_ip_address": "172.31.36.xxx",
        "ec2_public_dns_name": "ec2-54-194-xx-xx.eu-west-1.compute.amazonaws.com",
        "ec2_ramdisk": "",
        "ec2_reason": "",
        "ec2_region": "eu-west-1",
        "ec2_requester_id": "",
        "ec2_root_device_name": "/dev/sda1",
        "ec2_root_device_type": "ebs",
        "ec2_security_group_ids": "sg-0002a09792ce99ae6",
        "ec2_security_group_names": "launch-wizard-1",
        "ec2_sourceDestCheck": "true",
        "ec2_spot_instance_request_id": "",
        "ec2_state": "running",
        "ec2_state_code": 16,
        "ec2_state_reason": "",
        "ec2_subnet_id": "subnet-a2b952f8",
        "ec2_virtualization_type": "hvm",
        "ec2_vpc_id": "vpc-b0f39cd6"
      }
    }
  },
  "ami_08d658f84a6d84a80": [
    "54.194.xx.xx"
  ],
  "ec2": [
    "54.194.xx.xx"
  ],
  "eu-west-1": [
    "54.194.xx.xx"
  ],
  "eu-west-1a": [
    "54.194.xx.xx"
  ],
  "i-0cf2dc8949370619b": [
    "54.194.xx.xx"
  ],
  "key_aws": [
    "54.194.xx.xx"
  ],
  "platform_undefined": [
    "54.194.xx.xx"
  ],
  "security_group_launch_wizard_1": [
    "54.194.xx.xx"
  ],
  "tag_none": [
    "54.194.xx.xx"
  ],
  "type_t2_medium": [
    "54.194.xx.xx"
  ],
  "vpc_id_vpc_b0f39cd6": [
    "54.194.xx.xx"
  ]
}
```